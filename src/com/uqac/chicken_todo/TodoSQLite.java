package com.uqac.chicken_todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-13
 * Time: 09:35
 * To change this template use File | Settings | File Templates.
 */
public class TodoSQLite extends SQLiteOpenHelper {
    static final String db_name = "db.todo";
    static final String table_name = "tTodo";
    static final String[] columns = new String[6];

    static {
        columns[0] = "_id";
        columns[1] = "name";
        columns[2] = "tags";
        columns[3] = "filepath";
        columns[4] = "timestamp";
        columns[5] = "duration";
    }

    public TodoSQLite(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, db_name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + table_name + "( " +
                columns[0] + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                columns[1] + " TEXT NOT NULL, " +
                columns[2] + " TEXT, " +
                columns[3] + " TEXT NOT NULL, " +
                columns[4] + " INTEGER NOT NULL, " +
                columns[5] + " INTEGER NOT NULL)");
        ContentValues v = new ContentValues(3);
        v.put(columns[1], "TEST name");
        v.put(columns[2], "tags test lolo");
        v.put(columns[3], "0123456789");
        v.put(columns[4], System.currentTimeMillis());
        v.put(columns[5], 5);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        Log.v("TodoSQLite", "Cannot upgrade database, skipping.");
    }
}
