package com.uqac.chicken_todo;

import android.*;
import android.R;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-13
 * Time: 19:10
 * To change this template use File | Settings | File Templates.
 */
public class RecordButton extends Button implements View.OnClickListener {

    enum State {
        IDLE("Record", R.drawable.ic_btn_speak_now),
        RECORDING("Stop", R.drawable.ic_media_pause);

        String text;
        int icon_id;
        State(String text, int icon_id) {
            this.text = text;
            this.icon_id = icon_id;
        }
    }
    State state = State.IDLE;

    public interface RecordListener {
        void onStartRecording(MediaRecorder recorder);
        void onStopRecording(MediaRecorder recorder, long duration);
    }
    RecordListener listener;
    public void setOnRecordListener(RecordListener listener) {
        this.listener = listener;
    }

    MediaRecorder recorder = new MediaRecorder();
    int maxDuration = 30*1000;

    public RecordButton(Context context) {
        super(context);
        setOnClickListener(this);
        initRecorder();
        update();
    }
    public RecordButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
        initRecorder();
        update();
    }
    public RecordButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnClickListener(this);
        initRecorder();
        update();
    }

    void initRecorder()
    {
        recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mediaRecorder, int i, int i2) {
                if (i == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
                    Toast.makeText(getContext(), "Maximum duration reached (" + String.valueOf(maxDuration/1000) + "s).", 3000).show();
                    onClick(RecordButton.this); // stop this;
                }
            }
        });
    }

    void update() {
        this.setText(state.text);
        this.setCompoundDrawablesWithIntrinsicBounds(0, state.icon_id, 0, 0);
    }

    long start_time;
    @Override
    public void onClick(View view) {
        switch (state) {
            case IDLE:
                start_time = System.currentTimeMillis();
                if (listener != null) listener.onStartRecording(recorder);
                state = State.RECORDING;
                break;
            case RECORDING:
                long elapsed = System.currentTimeMillis() - start_time;
                if (listener != null) listener.onStopRecording(recorder, elapsed);
                state = State.IDLE;
                break;
        }
        update();
    }
}
