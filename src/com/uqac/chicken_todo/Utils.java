package com.uqac.chicken_todo;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-13
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
