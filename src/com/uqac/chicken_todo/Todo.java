package com.uqac.chicken_todo;

import android.*;
import android.R;
import android.database.Cursor;
import android.media.AudioRecord;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-13
 * Time: 09:51
 * To change this template use File | Settings | File Templates.
 */
public class Todo {
    int id;
    String name;
    String tags;
    String filepath;
    public long duration;
    public long timestamp;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public static Todo fromCursor(Cursor c) {
        Todo t = new Todo();
        t.id = c.getInt(0);
        t.name = c.getString(1);
        t.tags = c.getString(2);
        t.filepath = c.getString(3);
        t.duration = c.getInt(4);
        t.timestamp = c.getInt(5);
        return t;
    }
}
