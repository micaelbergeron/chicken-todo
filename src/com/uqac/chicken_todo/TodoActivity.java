package com.uqac.chicken_todo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TodoActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    TextView txtRecordTodoName;
    RecordButton btnRecord;
    ListView lstTodo;

    MediaPlayer player = new MediaPlayer();
    CursorAdapter adapter;
    TodoDB db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        db = new TodoDB(this);
        db.open();

        txtRecordTodoName = (TextView)findViewById(R.id.txtRecordTodoName);
        btnRecord = (RecordButton)findViewById(R.id.btnRecord);
        btnRecord.setOnRecordListener(new RecordButton.RecordListener() {
            Todo todo;

            @Override
            public void onStartRecording(MediaRecorder recorder) {
                final String ext = ".3gp";
                todo = new Todo();
                todo.name = txtRecordTodoName.getText().toString();
                String safePath = getExternalFilesDir(null).getAbsolutePath() + "/" + getSafeName();
                todo.filepath = safePath;

                File f = new File(todo.filepath + ext);
                int i = 1;
                while(f.exists()) {
                    todo.filepath = safePath + String.format("_%d", i);
                    f = new File(todo.filepath + ext);
                    i++;
                }

                todo.filepath += ext;

                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder.setAudioSamplingRate(16000);
                recorder.setMaxDuration(30*1000);
                recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);

                recorder.setOutputFile(todo.filepath);
                try {
                    recorder.prepare();
                    recorder.start();
                } catch (IOException e) {
                    System.out.println("Cannot prepare recorder!");
                }
            }

            @Override
            public void onStopRecording(MediaRecorder recorder, long duration) {
                recorder.stop();
                recorder.reset();

                todo.duration = duration;
                todo.timestamp = System.currentTimeMillis();
                if (db.insertTodo(todo)) {
                    Toast.makeText(TodoActivity.this, "Save successful!", 3000).show();
                    update();
                }
            }
        });
        lstTodo = (ListView)findViewById(R.id.lstTodo);

        bindTodos();
    }

    synchronized void update() {
        adapter.changeCursor(db.getListCursor());
    }

    void bindTodos() {
        String[] from = new String[3];
        from[0] = "name";
        from[1] = "filepath";
        from[2] = "duration";

        int[] to = new int[3];
        to[0] = R.id.txtName;
        to[1] = R.id.txtDetail;
        to[2] = R.id.txtDuration;

        adapter = new SimpleCursorAdapter(this, R.layout.item_todo, db.getListCursor(), from, to, 0);
        lstTodo.setAdapter(adapter);
        lstTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SQLiteCursor o = (SQLiteCursor)adapterView.getItemAtPosition(i);
                final Todo todo = Todo.fromCursor(o);
                File f = new File(todo.filepath);
                if (!f.exists()) {
                    askForDeleteUnexistingFile(todo);
                    return;
                }
                try {
                    player.reset();
                    player.setDataSource(todo.filepath);
                    player.prepare();
                    player.start();
                } catch (IOException e) {
                    System.out.println("Trying to play the file.");
                }
            }
        });

        lstTodo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                SQLiteCursor cursor = (SQLiteCursor)adapterView.getItemAtPosition(i);
                final Todo todo = Todo.fromCursor(cursor);
                askForDelete(todo);
                return true;
            }
        });
    }

    String getSafeName() {
        String unsafeName = txtRecordTodoName.getText().toString();
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
            byte[] digested = digest.digest(unsafeName.getBytes("UTF-8"));
            String safeName = Utils.bytesToHex(digested);

            return safeName;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    void askForDeleteUnexistingFile(final Todo todo) {
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case Dialog.BUTTON_POSITIVE:
                        db.deleteTodo(todo);
                        update();
                        break;
                    case Dialog.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        Dialog dialog = new AlertDialog.Builder(TodoActivity.this)
                .setMessage("The todo file couldn't be found, do you want to remove it from this list?")
                .setTitle("Missing TODO")
                .setPositiveButton("Yes", dialogListener)
                .setNegativeButton("No", dialogListener)
                .create();
        dialog.show();
    }

    void askForDelete(final Todo todo) {
        final DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        File f = new File(todo.filepath);
                        f.delete();
                        db.deleteTodo(todo.id);
                        update();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        // do nothing.
                        break;
                }
            }
        };

        Dialog dialog = new AlertDialog.Builder(TodoActivity.this)
                .setMessage("Do you want to delete this todo?")
                .setTitle("Delete TODO")
                .setPositiveButton("Yes", dialogListener)
                .setNegativeButton("No", dialogListener)
                .create();
        dialog.show();
    }
}
