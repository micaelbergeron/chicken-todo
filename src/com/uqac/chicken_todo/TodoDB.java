package com.uqac.chicken_todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created with IntelliJ IDEA.
 * User: micae_000
 * Date: 13-04-13
 * Time: 10:34
 * To change this template use File | Settings | File Templates.
 */
public class TodoDB {
    TodoSQLite db_helper;
    SQLiteDatabase db;

    public TodoDB(Context ctx) {
        //ctx.deleteDatabase(TodoSQLite.db_name);
        db_helper = new TodoSQLite(ctx, null, 1);
    }

    public synchronized void open() {
        db = db_helper.getWritableDatabase();
    }
    public synchronized void close() {
        db.close();
    }

    public synchronized Cursor getListCursor() {
        String[] select = TodoSQLite.columns.clone();
        select[5] = "CAST(" + select[5] + "/1000 AS TEXT) || 'sec.' AS duration" ;
        return db.query(TodoSQLite.table_name, select, null, null, null, null, null);
    }

    public synchronized boolean insertTodo(Todo t) {
        ContentValues values = new ContentValues();
        values.put(TodoSQLite.columns[1], t.name);
        values.put(TodoSQLite.columns[2], t.tags);
        values.put(TodoSQLite.columns[3], t.filepath);
        values.put(TodoSQLite.columns[4], t.timestamp);
        values.put(TodoSQLite.columns[5], t.duration);

        return db.insert(TodoSQLite.table_name, null, values) > 0;
    }

    public synchronized boolean deleteTodo(int id) {
        return db.delete(TodoSQLite.table_name, String.format("_id = %d", id), null) > 0;
    }
    public synchronized boolean deleteTodo(Todo t) {
        return deleteTodo(t.id);
    }

    public synchronized boolean updateTodo(Todo t) {
        return false;
    }
}
